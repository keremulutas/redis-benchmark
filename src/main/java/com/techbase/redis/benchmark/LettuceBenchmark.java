package com.techbase.redis.benchmark;

import com.google.common.collect.Lists;
import com.lambdaworks.redis.ClientOptions;
import com.lambdaworks.redis.LettuceFutures;
import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisFuture;
import com.lambdaworks.redis.RedisURI;
import com.lambdaworks.redis.SocketOptions;
import com.lambdaworks.redis.api.StatefulRedisConnection;
import com.lambdaworks.redis.api.async.RedisAsyncCommands;
import com.lambdaworks.redis.api.sync.RedisCommands;
import com.lambdaworks.redis.codec.ByteArrayCodec;
import com.lambdaworks.redis.support.ConnectionPoolSupport;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * Created by kerem on 13.03.2017.
 */
public class LettuceBenchmark {
    private boolean syncTestEnabled;
    private boolean syncMultiTestEnabled;
    private boolean asyncTestEnabled;
    private boolean unixSocketTestEnabled;

    private int iterations;
    private String keyPrefix;

    private String redisHost;
    private int redisPort;
    private int dbIndex;

    private int defaultTimeout;
    private int connectTimeout;
    private boolean tcpNoDelay;

    private String redisUnixSocket;

    private static RedisURI redisURI;

    public LettuceBenchmark(final Properties prop) {
        this.iterations = Integer.parseInt(prop.getProperty("iterations", "1000000"), 10);
        this.keyPrefix = prop.getProperty("keyPrefix", "benchmark-");

        this.redisHost = prop.getProperty("redisHost", "10.19.90.2");
        this.redisPort = Integer.parseInt(prop.getProperty("redisPort", "10000"), 10);
        this.dbIndex = Integer.parseInt(prop.getProperty("redisDbIndex", "0"), 10);

        this.redisUnixSocket = prop.getProperty("redisUnixSocket");

        this.syncTestEnabled = Boolean.parseBoolean(prop.getProperty("lettuce.sync.enabled", "false"));
        this.syncMultiTestEnabled = Boolean.parseBoolean(prop.getProperty("lettuce.sync-multi.enabled", "true"));
        this.asyncTestEnabled = Boolean.parseBoolean(prop.getProperty("lettuce.async.enabled", "true"));
        this.unixSocketTestEnabled = Boolean.parseBoolean(prop.getProperty("lettuce.unixsocket.enabled", "false"));
        this.defaultTimeout = Integer.parseInt(prop.getProperty("lettuce.defaultTimeout", "600"), 10);
        this.connectTimeout = Integer.parseInt(prop.getProperty("lettuce.connectTimeout", "30"), 10);
        this.tcpNoDelay = Boolean.parseBoolean(prop.getProperty("lettuce.tcpNoDelay", "true"));
    }

    private GenericObjectPool<StatefulRedisConnection<byte[], byte[]>> pool = ConnectionPoolSupport
            .createGenericObjectPool(new Supplier<StatefulRedisConnection<byte[], byte[]>>() {
                     @Override
                     public StatefulRedisConnection<byte[], byte[]> get() {
                         RedisClient redisClient = RedisClient.create(redisURI);
                         redisClient.setDefaultTimeout(defaultTimeout, TimeUnit.MILLISECONDS);
                         ClientOptions co = ClientOptions.builder()
                                 .socketOptions(SocketOptions.builder()
                                         .connectTimeout(connectTimeout, TimeUnit.SECONDS)
                                         .tcpNoDelay(tcpNoDelay)
                                         .build())
                                 .build();
                         redisClient.setOptions(co);

                         return redisClient.connect(new ByteArrayCodec());
                     }
                 },
                 new GenericObjectPoolConfig());

    private void runSyncTest() {
        RedisCommands<byte[], byte[]> sync;

        try {
            StatefulRedisConnection<byte[], byte[]> connection = this.pool.borrowObject();
            sync = connection.sync();

            long startTime = System.currentTimeMillis();

            for (int i = 1; i <= this.iterations; i++) {
                byte[] key = (this.keyPrefix + "key-" + i).getBytes();
                byte[] val = ("value-" + i).getBytes();

                sync.set(key, val);

                System.out.print("Testing sync set cmd performance... " + i + " / " + this.iterations + "\r");
            }
            System.out.println("");

            long endTime   = System.currentTimeMillis();
            long totalTime = endTime - startTime;

            System.out.println((totalTime / 1000.0) + " seconds / " + (this.iterations / (totalTime / 1000.0)) + " req per sec");

            System.out.println("");
            long startTime2 = System.currentTimeMillis();

            for (int i = 1; i <= this.iterations; i++) {
                byte[] key = (this.keyPrefix + "key-" + i).getBytes();

                sync.del(key);
                System.out.print("Testing sync del cmd performance... " + i + " / " + this.iterations + "\r");
            }
            System.out.println("");

            long endTime2   = System.currentTimeMillis();
            long totalTime2 = endTime2 - startTime2;

            System.out.println((totalTime2 / 1000.0) + " seconds / " + (this.iterations / (totalTime2 / 1000.0)) + " req per sec");
            System.out.println("");
            System.out.println("==============================");
            System.out.println("");
        } catch (Exception e) {
            System.out.println("");
            System.out.println("");
            e.printStackTrace();
            System.out.println("");
            System.out.println("");
        }
    }

    private void runSyncMultiTest() {
        RedisCommands<byte[], byte[]> sync;

        try {
            StatefulRedisConnection<byte[], byte[]> connection = this.pool.borrowObject();
            sync = connection.sync();

            long startTime = System.currentTimeMillis();

            sync.multi();
            for (int i = 1; i <= this.iterations; i++) {
                byte[] key = (this.keyPrefix + "key-" + i).getBytes();
                byte[] val = ("value-" + i).getBytes();

                sync.set(key, val);
                System.out.print("Testing sync multi set cmd performance... " + i + " / " + this.iterations + "\r");
            }
            System.out.println("");

            sync.exec();

            long endTime   = System.currentTimeMillis();
            long totalTime = endTime - startTime;

            System.out.println((totalTime / 1000.0) + " seconds / " + (this.iterations / (totalTime / 1000.0)) + " req per sec");
            System.out.println("");

            long startTime2 = System.currentTimeMillis();

            sync.multi();
            for (int i = 1; i <= this.iterations; i++) {
                byte[] key = (this.keyPrefix + "key-" + i).getBytes();

                sync.del(key);
                System.out.print("Testing sync multi del cmd performance... " + i + " / " + this.iterations + "\r");
            }
            System.out.println("");

            sync.exec();

            long endTime2   = System.currentTimeMillis();
            long totalTime2 = endTime2 - startTime2;

            System.out.println((totalTime2 / 1000.0) + " seconds / " + (this.iterations / (totalTime2 / 1000.0)) + " req per sec");
            System.out.println("");
            System.out.println("==============================");
            System.out.println("");
        } catch (Exception e) {
            System.out.println("");
            System.out.println("");
            e.printStackTrace();
            System.out.println("");
            System.out.println("");
        }
    }

    private void runAsyncTest() {
        RedisAsyncCommands<byte[], byte[]> async;

        try {
            StatefulRedisConnection<byte[], byte[]> connection = this.pool.borrowObject();
            async = connection.async();

            async.setAutoFlushCommands(false);

            long startTime = System.currentTimeMillis();

            List<RedisFuture<?>> futuresSet = Lists.newArrayList();
            for (int i = 1; i <= this.iterations; i++) {
                byte[] key = (this.keyPrefix + "key-" + i).getBytes();
                byte[] val = ("value-" + i).getBytes();

                futuresSet.add(async.set(key, val));
                System.out.print("Testing async set cmd performance... " + i + " / " + iterations + "\r");
            }
            System.out.println("");

            async.flushCommands();

            boolean resultSet = LettuceFutures.awaitAll(
                    3000,
                    TimeUnit.SECONDS,
                    futuresSet.toArray(new RedisFuture[futuresSet.size()])
            );

            long endTime = System.currentTimeMillis();
            long totalTime = endTime - startTime;

//            System.out.println("set result: " + resultSet);
            System.out.println((totalTime / 1000.0) + " seconds / " + (this.iterations / (totalTime / 1000.0)) + " req per sec");
            System.out.println("");

            long startTime2 = System.currentTimeMillis();

            List<RedisFuture<?>> futuresDel = Lists.newArrayList();
            for (int i = 1; i <= this.iterations; i++) {
                byte[] key = (this.keyPrefix + "key-" + i).getBytes();

                futuresDel.add(async.del(key));
                System.out.print("Testing async set del performance... " + i + " / " + this.iterations + "\r");
            }
            System.out.println("");

            async.flushCommands();

            boolean resultDel = LettuceFutures.awaitAll(3000, TimeUnit.SECONDS,
                    futuresDel.toArray(new RedisFuture[futuresDel.size()]));

            long endTime2 = System.currentTimeMillis();
            long totalTime2 = endTime2 - startTime2;

//            System.out.println("delete result: " + resultDel);
            System.out.println((totalTime2 / 1000.0) + " seconds / " + (this.iterations / (totalTime2 / 1000.0)) + " req per sec");
            System.out.println("");
            System.out.println("==============================");
            System.out.println("");
        } catch (Exception e) {
            System.out.println("");
            System.out.println("");
            e.printStackTrace();
            System.out.println("");
            System.out.println("");
        }
    }

    public void runBenchmark() {
        redisURI = RedisURI.builder()
                .withHost(this.redisHost)
                .withPort(this.redisPort)
                .withDatabase(this.dbIndex)
                .build();

        System.out.println("Running Lettuce TCP connection tests..");
        System.out.println("==============================");
        System.out.println("");

        if(this.syncMultiTestEnabled) {
            runSyncMultiTest();
        }
        if(this.asyncTestEnabled) {
            runAsyncTest();
        }
        if(this.syncTestEnabled) {
            runSyncTest();
        }

        if(this.unixSocketTestEnabled && this.redisUnixSocket != null && !this.redisUnixSocket.equals("")) {
            redisURI = RedisURI.builder()
                    .socket(this.redisUnixSocket)
                    .build();

            System.out.println("Running Unix socket connection tests..");
            System.out.println("======================================");
            System.out.println("");

            if(this.syncMultiTestEnabled) {
                runSyncMultiTest();
            }
            if(this.asyncTestEnabled) {
                runAsyncTest();
            }
            if(this.syncTestEnabled) {
                runSyncTest();
            }
        }

        pool.close();
    }
}
