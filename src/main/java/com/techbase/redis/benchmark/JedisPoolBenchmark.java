package com.techbase.redis.benchmark;

import redis.clients.jedis.*;

import java.util.Properties;

/**
 * Created by kerem on 15.03.2017.
 */
public class JedisPoolBenchmark {
    private boolean singleConnectionTestEnabled;
    private boolean multiConnectionTestEnabled;

    private int iterations;
    private String keyPrefix;

    private String redisHost;
    private int redisPort;
    private int dbIndex;

    private JedisPool jedisPool;
    private int maxIdle;
    private int minIdle;
    private int maxTotal;
    private int timeout;
    private int maxWaitMillis;
    private boolean blockWhenExhausted;
    private boolean lifo;
    private boolean testOnCreate;
    private boolean testOnBorrow;
    private boolean testOnReturn;
    private boolean testWhileIdle;

    public JedisPoolBenchmark(final Properties prop) {
        this.maxIdle = Integer.parseInt(prop.getProperty("jedis.pool.maxIdle", "4"), 10);
        this.minIdle = Integer.parseInt(prop.getProperty("jedis.pool.minIdle", "2"), 10);
        this.maxTotal = Integer.parseInt(prop.getProperty("jedis.pool.maxTotal", "8"), 10);
        this.maxWaitMillis = Integer.parseInt(prop.getProperty("jedis.pool.maxWaitMillis", "-1"), 10);
        this.timeout = Integer.parseInt(prop.getProperty("jedis.pool.timeout", "30"), 10);
        this.blockWhenExhausted = Boolean.parseBoolean(prop.getProperty("jedis.pool.blockWhenExhausted", "true"));
        this.lifo = Boolean.parseBoolean(prop.getProperty("jedis.pool.lifo", "false"));
        this.testOnCreate = Boolean.parseBoolean(prop.getProperty("jedis.pool.testOnCreate", "true"));
        this.testOnBorrow = Boolean.parseBoolean(prop.getProperty("jedis.pool.testOnBorrow", "true"));
        this.testOnReturn = Boolean.parseBoolean(prop.getProperty("jedis.pool.testOnReturn", "true"));
        this.testWhileIdle = Boolean.parseBoolean(prop.getProperty("jedis.pool.testWhileIdle", "true"));

        this.iterations = Integer.parseInt(prop.getProperty("iterations", "1000000"), 10);
        this.keyPrefix = prop.getProperty("keyPrefix", "benchmark-");

        this.redisHost = prop.getProperty("redisHost", "10.19.90.2");
        this.redisPort = Integer.parseInt(prop.getProperty("redisPort", "10000"), 10);
        this.dbIndex = Integer.parseInt(prop.getProperty("redisDbIndex", "0"), 10);

        this.singleConnectionTestEnabled = Boolean.parseBoolean(prop.getProperty("jedis.pool.singleconnection.enabled", "true"));
        this.multiConnectionTestEnabled = Boolean.parseBoolean(prop.getProperty("jedis.pool.multiconnection.enabled", "true"));
    }

    private void runSingleConnectionTest() {
        Jedis jedis = jedisPool.getResource();

        try {
            long startTime = System.currentTimeMillis();

            for (int i = 1; i <= iterations; i++) {
                byte[] key = (keyPrefix + "key-" + i).getBytes();
                byte[] val = ("value-" + i).getBytes();

                jedis.set(key, val);

                System.out.print("Testing single connection set cmd performance... " + i + " / " + iterations + "\r");
            }
            System.out.println("");

            long endTime   = System.currentTimeMillis();
            long totalTime = endTime - startTime;

            System.out.println((totalTime / 1000.0) + " seconds / " + (iterations / (totalTime / 1000.0)) + " req per sec");

            System.out.println("");

            jedis.close();

            jedis = jedisPool.getResource();
            long startTime2 = System.currentTimeMillis();

            for (int i = 1; i <= iterations; i++) {
                byte[] key = (keyPrefix + "key-" + i).getBytes();

                jedis.del(key);
                System.out.print("Testing single connection del cmd performance... " + i + " / " + iterations + "\r");
            }
            System.out.println("");

            long endTime2   = System.currentTimeMillis();
            long totalTime2 = endTime2 - startTime2;

            System.out.println((totalTime2 / 1000.0) + " seconds / " + (iterations / (totalTime2 / 1000.0)) + " req per sec");
            System.out.println("");
            System.out.println("==============================");
            System.out.println("");
            jedis.close();
        } catch (Exception e) {
            System.out.println("");
            System.out.println("");
            e.printStackTrace();
            System.out.println("");
            System.out.println("");
        }
    }

    private void runMultiConnectionTest() {
        try {
            long startTime = System.currentTimeMillis();

            for (int i = 1; i <= iterations; i++) {
                Jedis jedis = jedisPool.getResource();
                byte[] key = (keyPrefix + "key-" + i).getBytes();
                byte[] val = ("value-" + i).getBytes();

                jedis.set(key, val);
                jedis.close();

                System.out.print("Testing multi connection set cmd performance... " + i + " / " + iterations + "\r");
            }
            System.out.println("");

            long endTime   = System.currentTimeMillis();
            long totalTime = endTime - startTime;

            System.out.println((totalTime / 1000.0) + " seconds / " + (iterations / (totalTime / 1000.0)) + " req per sec");

            System.out.println("");

            long startTime2 = System.currentTimeMillis();

            for (int i = 1; i <= iterations; i++) {
                Jedis jedis = jedisPool.getResource();
                byte[] key = (keyPrefix + "key-" + i).getBytes();

                jedis.del(key);
                jedis.close();
                System.out.print("Testing multi connection del cmd performance... " + i + " / " + iterations + "\r");
            }
            System.out.println("");

            long endTime2   = System.currentTimeMillis();
            long totalTime2 = endTime2 - startTime2;

            System.out.println((totalTime2 / 1000.0) + " seconds / " + (iterations / (totalTime2 / 1000.0)) + " req per sec");
            System.out.println("");
            System.out.println("==============================");
            System.out.println("");
        } catch (Exception e) {
            System.out.println("");
            System.out.println("");
            e.printStackTrace();
            System.out.println("");
            System.out.println("");
        }
    }

    public void runBenchmark() {
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxIdle(this.maxIdle);
        poolConfig.setMinIdle(this.minIdle);
        poolConfig.setMaxTotal(this.maxTotal);
        poolConfig.setLifo(this.lifo);
        poolConfig.setBlockWhenExhausted(this.blockWhenExhausted);
        poolConfig.setMaxWaitMillis(this.maxWaitMillis);
        poolConfig.setTestOnCreate(this.testOnCreate);
        poolConfig.setTestOnBorrow(this.testOnBorrow);
        poolConfig.setTestOnReturn(this.testOnReturn);
        poolConfig.setTestWhileIdle(this.testWhileIdle);
        jedisPool = new JedisPool(poolConfig, this.redisHost, this.redisPort, this.timeout, null, this.dbIndex, null);

        System.out.println("Running Jedis Pool TCP connection tests..");
        System.out.println("==============================");
        System.out.println("");

        if(this.singleConnectionTestEnabled) {
            runSingleConnectionTest();
        }
        if(this.multiConnectionTestEnabled) {
            runMultiConnectionTest();
        }
    }
}
