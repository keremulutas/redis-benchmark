package com.techbase.redis.benchmark;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Transaction;

import java.util.Properties;

/**
 * Created by kerem on 13.03.2017.
 */
public class JedisBenchmark {
    private boolean syncTestEnabled;
    private boolean syncMultiTestEnabled;
    private boolean pipelineTestEnabled;

    private int iterations;
    private String keyPrefix;

    private String redisHost;
    private int redisPort;
    private int connectionTimeout;
    private int socketTimeout;

    private Jedis jedis;

    public JedisBenchmark(final Properties prop) {
        this.iterations = Integer.parseInt(prop.getProperty("iterations", "1000000"), 10);
        this.keyPrefix = prop.getProperty("keyPrefix", "benchmark-");

        this.redisHost = prop.getProperty("redisHost", "10.19.90.2");
        this.redisPort = Integer.parseInt(prop.getProperty("redisPort", "10000"), 10);
        this.connectionTimeout = Integer.parseInt(prop.getProperty("jedis.connectionTimeout", "30000"), 10);
        this.socketTimeout = Integer.parseInt(prop.getProperty("jedis.socketTimeout", "30000"), 10);

        this.syncTestEnabled = Boolean.parseBoolean(prop.getProperty("jedis.sync.enabled", "false"));
        this.syncMultiTestEnabled = Boolean.parseBoolean(prop.getProperty("jedis.sync-multi.enabled", "true"));
        this.pipelineTestEnabled = Boolean.parseBoolean(prop.getProperty("jedis.pipeline.enabled", "true"));
    }

    private void runSyncTest() {
        try {
            long startTime = System.currentTimeMillis();

            for (int i = 1; i <= iterations; i++) {
                byte[] key = (keyPrefix + "key-" + i).getBytes();
                byte[] val = ("value-" + i).getBytes();

                this.jedis.set(key, val);

                System.out.print("Testing sync set cmd performance... " + i + " / " + this.iterations + "\r");
            }
            System.out.println("");

            long endTime   = System.currentTimeMillis();
            long totalTime = endTime - startTime;

            System.out.println((totalTime / 1000.0) + " seconds / " + (this.iterations / (totalTime / 1000.0)) + " req per sec");

            System.out.println("");
            long startTime2 = System.currentTimeMillis();

            for (int i = 1; i <= this.iterations; i++) {
                byte[] key = (this.keyPrefix + "key-" + i).getBytes();

                this.jedis.del(key);
                System.out.print("Testing sync del cmd performance... " + i + " / " + this.iterations + "\r");
            }
            System.out.println("");

            long endTime2   = System.currentTimeMillis();
            long totalTime2 = endTime2 - startTime2;

            System.out.println((totalTime2 / 1000.0) + " seconds / " + (this.iterations / (totalTime2 / 1000.0)) + " req per sec");
            System.out.println("");
            System.out.println("==============================");
            System.out.println("");
        } catch (Exception e) {
            System.out.println("");
            System.out.println("");
            e.printStackTrace();
            System.out.println("");
            System.out.println("");
        }
    }

    private void runSyncMultiTest() {
        try {
            long startTime = System.currentTimeMillis();

            Transaction t = this.jedis.multi();
            for (int i = 1; i <= this.iterations; i++) {
                byte[] key = (this.keyPrefix + "key-" + i).getBytes();
                byte[] val = ("value-" + i).getBytes();

                t.set(key, val);
                System.out.print("Testing sync multi set cmd performance... " + i + " / " + this.iterations + "\r");
            }
            System.out.println("");

            t.exec();

            long endTime   = System.currentTimeMillis();
            long totalTime = endTime - startTime;

            System.out.println((totalTime / 1000.0) + " seconds / " + (this.iterations / (totalTime / 1000.0)) + " req per sec");
            System.out.println("");

            long startTime2 = System.currentTimeMillis();

            Transaction t2 = this.jedis.multi();
            for (int i = 1; i <= this.iterations; i++) {
                byte[] key = (this.keyPrefix + "key-" + i).getBytes();

                t2.del(key);
                System.out.print("Testing sync multi del cmd performance... " + i + " / " + this.iterations + "\r");
            }
            System.out.println("");

            t2.exec();

            long endTime2   = System.currentTimeMillis();
            long totalTime2 = endTime2 - startTime2;

            System.out.println((totalTime2 / 1000.0) + " seconds / " + (this.iterations / (totalTime2 / 1000.0)) + " req per sec");
            System.out.println("");
            System.out.println("==============================");
            System.out.println("");
        } catch (Exception e) {
            System.out.println("");
            System.out.println("");
            e.printStackTrace();
            System.out.println("");
            System.out.println("");
        }
    }

    private void runPipelineTest() {
        try {
            long startTime = System.currentTimeMillis();

            Pipeline p = this.jedis.pipelined();
            for (int i = 1; i <= this.iterations; i++) {
                byte[] key = (this.keyPrefix + "key-" + i).getBytes();
                byte[] val = ("value-" + i).getBytes();

                p.set(key, val);
                System.out.print("Testing pipeline set cmd performance... " + i + " / " + this.iterations + "\r");
            }
            System.out.println("");

            p.sync();

            long endTime = System.currentTimeMillis();
            long totalTime = endTime - startTime;

            System.out.println((totalTime / 1000.0) + " seconds / " + (this.iterations / (totalTime / 1000.0)) + " req per sec");
            System.out.println("");

            long startTime2 = System.currentTimeMillis();

            Pipeline p2 = this.jedis.pipelined();
            for (int i = 1; i <= this.iterations; i++) {
                byte[] key = (this.keyPrefix + "key-" + i).getBytes();

                p2.del(key);
                System.out.print("Testing pipeline set del performance... " + i + " / " + this.iterations + "\r");
            }
            System.out.println("");

            p2.sync();

            long endTime2 = System.currentTimeMillis();
            long totalTime2 = endTime2 - startTime2;

            System.out.println((totalTime2 / 1000.0) + " seconds / " + (this.iterations / (totalTime2 / 1000.0)) + " req per sec");
            System.out.println("");
            System.out.println("==============================");
            System.out.println("");
        } catch (Exception e) {
            System.out.println("");
            System.out.println("");
            e.printStackTrace();
            System.out.println("");
            System.out.println("");
        }
    }

    public void runBenchmark() {
        jedis = new Jedis(this.redisHost, this.redisPort, this.connectionTimeout, this.socketTimeout);

        System.out.println("Running Jedis TCP connection tests..");
        System.out.println("==============================");
        System.out.println("");

        if(this.syncMultiTestEnabled) {
            runSyncMultiTest();
        }
        if(this.pipelineTestEnabled) {
            runPipelineTest();
        }
        if(this.syncTestEnabled) {
            runSyncTest();
        }
    }
}
