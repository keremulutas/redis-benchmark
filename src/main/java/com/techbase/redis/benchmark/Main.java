package com.techbase.redis.benchmark;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by kerem on 11.03.2017.
 */
public class Main {
    public static void main(String... args) {
        if(args.length != 1) {
            System.out.println("This application requires the path for benchmark properties file.");
            System.exit(1);
        }

        Properties prop = new Properties();

        try (InputStream input = new FileInputStream(args[0])) {
            prop.load(input);
        } catch (FileNotFoundException e) {
            System.out.println("Could not load properties file.");
            e.printStackTrace();
            System.exit(1);
        } catch (IOException e) {
            System.out.println("IO exception occurred.");
            e.printStackTrace();
            System.exit(1);
        }

        boolean lettuceEnabled = Boolean.parseBoolean(prop.getProperty("lettuce.enabled", "false"));
        if(lettuceEnabled) {
            LettuceBenchmark lb = new LettuceBenchmark(prop);
            lb.runBenchmark();

            System.out.println("----------------------------------------------------------");
            System.out.println("----------------------------------------------------------");
            System.out.println("");
        }

        boolean jedisEnabled = Boolean.parseBoolean(prop.getProperty("jedis.enabled", "false"));
        if(jedisEnabled) {
            JedisBenchmark jb = new JedisBenchmark(prop);
            jb.runBenchmark();

            System.out.println("----------------------------------------------------------");
            System.out.println("----------------------------------------------------------");
            System.out.println("");
        }

        boolean jedisPoolEnabled = Boolean.parseBoolean(prop.getProperty("jedis.pool.enabled", "true"));
        if(jedisPoolEnabled) {
            JedisPoolBenchmark jpb = new JedisPoolBenchmark(prop);
            jpb.runBenchmark();
        }
    }
}
