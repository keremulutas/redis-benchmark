# Java Redis clients benchmark utility

Tests Lettuce & Jedis clients. Sample config file:

```
iterations=1000000
keyPrefix=benchmark-

redisHost=...
redisPort=...

# works only if lettuce.unixsocket.enabled set to true
redisUnixSocket=...

lettuce.sync.enabled=false
lettuce.sync-multi.enabled=true
lettuce.async.enabled=true
lettuce.unixsocket.enabled=false

jedis.sync.enabled=false
jedis.sync-multi.enabled=true
jedis.pipeline.enabled=true
```

Usage:

`java -jar redis-benchmark-1.0-SNAPSHOT.jar benchmark.properties`